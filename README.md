# cascade-label-test

> 级联选择分类按钮组

## 说明

1. 本项目为微信项目提供树形分类的选择界面，以及保存功能。
2. 支持两级分类

## Demo 查看

[demo 预览](http://raiseinfo.cn/cascade-button/#/)

![](http://raiseinfo.cn/cascade-button/demo.jpg)

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

```

![](http://raiseinfo.cn/cascade-button/weixin.jpg)
![](http://raiseinfo.cn/cascade-button/zhifubao.jpg)
