module.exports = {
  // 接口地址定义文件
  // 格式： 名称: 'URL'

  // 上传地址
  // uploadPath: window.SITE_CONFIG.baseUrl + '/admin/storage/create',
  uploadPath: '/sys/oss/upload',
  // fullUploadPath: window.SITE_CONFIG.baseUrl + '/sys/oss/upload',
  /**
   * 重要说明
   * 此处定义URL的别名，目的在于项目迭代中URL地址的变更，只需要在此几个文件中修改即可
   * 【段注释，必须，用于书名是那个模块的接口】
   * 接口变量命名约定：将URL转换为以驼峰方式命名，以便通过命名知道接口地址和接口功能
   */

  /**
   * 系统内置模块（段注释，说明是那个模块的接口）
   */

  /**
   * 演示数据接口
   */
  testList: '/getbuttonList',
  demoData: '/demo/data', // demo菜单中的演示数据接口地址
  demoDataList: '/demo/data-list', // 获取多行数据
  demoDataInfo: '/demo/data-info', // 获取对象数据
  demoDataPost: '/demo/data-post', // 提交数据
  getJsSDK: '/wechat/jsapiSignature', // 获取jsSDK

  /**
   * 其他业务模块
   */
  modulesApiUrlName: '', // 其他业务模块接口地址

  /**
   * 真实模拟接口
   */
  // 微信网页授权
  webAuthorize: '/wechat/webAuthorize',
  // 微信网页授权 code 换 accessToken
  oauth2AccessToken: '/wechat/oauth2AccessToken',
  // 获取文章list列表（分页）
  getArticleList: '/article/queryAll',
  // 获取所有文章（不分页）
  getAllArticleList: '/',
  // 获取我的文章类别（加品牌）
  getArticleTypeList: '/mycatalog/queryAll',
  // 获取所有的文章分类（不加品牌）
  getAllArticleTypeList: '/catalog/queryAll',
  // 获取所有子分类 (不加品牌)
  getChildTypeList: '/catalog/queryChildrenCatalog',
  // 保存我的分类
  saveMyType: '/mycatalog/save',
  // 根据文章标题获取文章
  getArticleByTitle: '/article/queryAll',
  // 根据文章类型获取文章
  getArticleByTypeId: '/article/queryAll',
  // 获取我的文章
  getMyArticle: '/article/queryAll',
  // 删除我的文章
  deleteMyArticle: '/myarticle/deleteMyArticle',
  // 获取我的热卖
  getMyGoods: '/myproduct/queryHotSaleProduct',
  // 获取文章信息
  getArticleInfo: '/article/info',
  // 保存文章
  saveArticle: '/article/save',
  // 修改文章
  updateArticle: '/article/update',
  // 获取客户统计
  getCustomerNum: '/customerleads/customerCount',
  // 保存客户关系
  saveCustomerLeads: '/customerleads/save',
  // 获取客户今日数据统计
  getCustomerTodayNum: '/customerleads/customerTodayCount',
  // 根据类型获取客服二维码的数据
  getServiceHelp: '/help/queryHelpType',
  // 获取客户list数据
  getCustomerList: '/customerleads/queryCustomerData',
  // 获取当前用户信息
  getUserInfo: '/wechatuser/info',
  // 获取我的客户的文章列表
  getCustomerArticleList: '/customerleads/queryCustomerDataInfo',
  // 修改个人用户信息
  updateUserInfo: '/wechatuser/update',
  // 修改姓名
  updateName: '/wechatuser/update',
  // 保存手机
  updatePhone: '/wechatuser/update',
  // 保存签名
  saveAutograph: '/saveAutograph',
  // 文章顶部是否显示我的名片和签名
  updateMyCard: '/updateMyCard',
  // 文章底部名片显示热卖商品
  updateGoods: '/updateGoods',
  // 修改欢迎词
  updateWelcome: '/wechatuser/update',
  // 获取我收藏的文章列表
  getCollectionArticleList: '/myfavorites/queryAll',
  // 获取我推荐的人的列表
  getReference: '/getReference',
  // 获取我的好友印象
  getMyImpression: '/impressionfriends/impressionQuery',
  // 保存我的好友印象
  saveImpression: '/impressionfriends/save',
  // 修改我的好友印象
  updateImpression: '/impressionfriends/update',
  // 删除我的好友印象
  deleteImpression: '/impressionfriends/delete',
  // 获取我的品牌列表
  getMyBrandList: '/mybrand/queryMyBrand',
  // 根据名称搜索品牌
  getBrandListByName: '/mybrand/queryCorporation',
  // 保存我的品牌
  saveMyBrand: '/mybrand/save',
  // 删除我的品牌
  deleteMybrand: '/mybrand/delete',
  // 获取早晚卡数据
  getSign: '/article/queryClockArticle',
  // 修改消息提醒的时间
  updateRemindTime: '/updateRemindTime',
  // 设置推送时间
  pushChange: '/wechatsettings/save',
  // 修改推送设置
  updateChange: '/wechatsettings/save',
  // 获取我的所有商品
  getGoodList: '/getGoodList',
  // 获取商品的具体信息
  getGoodInfo: '/myproduct/info',
  // 保存我的商品
  saveMyGoods: '/myproduct/save',
  // 置顶我的商品
  updateMyGoods: '/myproduct/update',
  // 删除我的商品
  deleteMyGoods: '/myproduct/delete',
  // 保存收藏文章
  savecollection: '/myfavorites/save',
  // 获取我的收藏
  getcollectionInfo: '/myfavorites/info',
  // 取消我的收藏
  deleteCollection: '/myfavorites/cancelFavorites',
  // 保存点赞
  savegoodArticle: '/mylike/save',
  // 获取文章是否点赞
  getgoodArticleInfo: '/mylike/info',
  // 取消点赞
  deletegoodArticle: '/mylike/cancelLike',
  // 获取投诉列表
  getComplaintList: '/complaint/queryCompalintCause',
  // 保存文章投诉
  saveComplaint: '/usercomplaint/save',
  // 获取我插入的内容
  getMyContent: '/articletemplate/info',
  // 获取我插入的所有存为模板
  getMyContentList: '/articletemplate/list',
  // 保存我插入的内容
  saveMyContent: '/articletemplate/save',
  // 修改我的所有内容
  updateMyContent: '/articletemplate/update',
  // 删除我的模板
  deleteMyContent: '/articletemplate/delete',
  // 获取我推荐的人的数据
  getMyRecommendPeople: '/wechatuser/queryMyRecommend',
  // 保存我的上传文件
  saveUpload: '/sys/oss/upload',
  // 获取个人的微信设置
  getPushSettingInfo: '/wechatsettings/info',
  // 获取硬广告
  getAdInfo: '/ad/queryImage',
  // 修改客户消息是否已读
  updateCustomerMessageRead: '/alertread/update',
  // 查看客户消息是否已读
  getCustomerMessageRead: '/alertread/queryAll'
}
