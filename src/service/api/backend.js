import http from '@/utils/httpRequest'
import api from '../api.js'

/**
 *  这里的接口为获取真实的模拟数据
 */

export function uploadFile (params) {
  return new Promise(function (resolve, reject) {
    let formData = new window.FormData()
    formData.append(params.name, params.file)
    http.request(api.uploadPath, formData, 'POST', 'file').then(res => {
      res.status === 1 ? resolve(res) : reject(res)
    })
  })
}
/**
 * 获取文章的list数据
 * @returns {Promise<any>}
 */
export function getArticleList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getArticleList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取文章的list数据
 * @returns {Promise<any>}
 */
export function getAllArticleList () {
  return new Promise(function (resolve, reject) {
    http.request(api.getAllArticleList).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的文章分类（加品牌）
 * @returns {Promise<any>}
 */
export function getArticleTypeList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getArticleTypeList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取所有的文章分类（不加品牌）
 * @returns {Promise<any>}
 */
export function getAllArticleTypeList () {
  return new Promise(function (resolve, reject) {
    http.request(api.getAllArticleTypeList).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 根据文章类型查文章
 * @param id
 * @returns {Promise<any>}
 */
export function getArticleByTypeId (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getArticleByTypeId, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 根据文章标题查文章列表
 * @param title
 * @returns {Promise<any>}
 */
export function getArticleByTitle (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getArticleByTitle, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取文章详情
 * @param id
 * @returns {Promise<any>}
 */
export function getArticleInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.getArticleInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的文章
 * @param id
 * @returns {Promise<any>}
 */
export function getMyArticle (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getMyArticle, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的文章
 * @param id
 * @returns {Promise<any>}
 */
export function deleteMyArticle (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.deleteMyArticle, params, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的热卖商品
 * @param id
 * @returns {Promise<any>}
 */
export function getMyGoods (wechatUid) {
  return new Promise(function (resolve, reject) {
    http.request(api.getMyGoods + '/' + wechatUid).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存文章数据
 * @param id
 * @returns {Promise<any>}
 */
export function saveArticle (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveArticle, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改文章数据
 * @param id
 * @returns {Promise<any>}
 */
export function updateArticle (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateArticle, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取客户统计数据
 * @returns {Promise<any>}
 * @constructor
 */
export function getCustomerNum (wechatUid) {
  return new Promise(function (resolve, reject) {
    http.request(api.getCustomerNum + '/' + wechatUid).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取今日客户统计数据
 * @returns {Promise<any>}
 * @constructor
 */
export function getCustomerTodayNum (wechatUid) {
  return new Promise(function (resolve, reject) {
    http.request(api.getCustomerTodayNum + '/' + wechatUid).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取客户列表数据
 * @param data
 * @returns {Promise<any>}
 * @constructor
 */
export function getCustomerList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getCustomerList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取客户列表数据
 * @param data
 * @returns {Promise<any>}
 * @constructor
 */
export function saveCustomerLeads (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveCustomerLeads, params, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取用户详细信息
 * @returns {Promise<any>}
 */
export function getUserInfo (openId) {
  return new Promise(function (resolve, reject) {
    http.request(api.getUserInfo + '/' + openId).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的客户文章列表
 * @returns {Promise<any>}
 */
export function getCustomerArticleList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getCustomerArticleList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取一个品牌
 * @returns {Promise<any>}
 */
export function getOneBrand () {
  return new Promise(function (resolve, reject) {
    http.request(api.getOneBrand).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取欢迎词
 * @returns {Promise<any>}
 */
export function getIntroduce () {
  return new Promise(function (resolve, reject) {
    http.request(api.getIntroduce).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改姓名
 * @param data
 * @returns {Promise<any>}
 */
export function updateUserInfo (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateUserInfo, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改姓名
 * @param data
 * @returns {Promise<any>}
 */
export function updateName (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateName, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存手机
 * @param data
 * @returns {Promise<any>}
 */
export function updatePhone (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updatePhone, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存签名
 * @param data
 * @returns {Promise<any>}
 */
export function saveAutograph (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveAutograph, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改文章顶部是否显示我的名片和签名
 * @param data
 * @returns {Promise<any>}
 */
export function updateMyCard (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateMyCard, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改文章顶部是否显示我的名片和签名
 * @param data
 * @returns {Promise<any>}
 */
export function updateGoods (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateGoods, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改文章顶部是否显示我的名片和签名
 * @param data
 * @returns {Promise<any>}
 */
export function updateWelcome (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateWelcome, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的收藏文章列表
 * @returns {Promise<any>}
 */
export function getCollectionArticleList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getCollectionArticleList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我推荐的人列表
 * @returns {Promise<any>}
 */
export function getReference () {
  return new Promise(function (resolve, reject) {
    http.request(api.getReference).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的好友印象
 * @returns {Promise<any>}
 */
export function getMyImpression (wechatUid) {
  return new Promise(function (resolve, reject) {
    http.request(api.getMyImpression + '/' + wechatUid).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存我的好友印象
 * @param data
 * @returns {Promise<any>}
 */
export function saveImpression (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveImpression, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改我的好友印象
 * @param data
 * @returns {Promise<any>}
 */
export function updateImpression (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateImpression, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 删除我的好友印象
 * @param data
 * @returns {Promise<any>}
 */
export function deleteImpression (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.deleteImpression, params, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的品牌
 * @returns {Promise<any>}
 */
export function getMyBrandList (wechatUid) {
  return new Promise(function (resolve, reject) {
    http.request(api.getMyBrandList + '/' + wechatUid).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 根据名称搜索品牌
 * @returns {Promise<any>}
 */
export function getBrandListByName (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getBrandListByName, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存我的品牌
 * @param data
 * @returns {Promise<any>}
 */
export function saveMyBrand (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveMyBrand, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 删除我的品牌
 * @param data
 * @returns {Promise<any>}
 */
export function deleteMybrand (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.deleteMybrand, params, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取打卡文章
 * @returns {Promise<any>}
 */
export function getSign () {
  return new Promise(function (resolve, reject) {
    http.request(api.getSign).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改几分钟消息提醒的时间
 * @param data
 * @returns {Promise<any>}
 */
export function updateRemindTime (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateRemindTime, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 设置推送时间
 * @param data
 * @returns {Promise<any>}
 */
export function pushChange (data) {
  return new Promise(function (resolve, reject) {
    http
      .request(api.pushChange, data, 'POST')
      .then(res => {
        res.code === 0 ? resolve(res) : reject(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * 设置推送时间
 * @param data
 * @returns {Promise<any>}
 */
export function updateChange (data) {
  return new Promise(function (resolve, reject) {
    http
      .request(api.updateChange, data, 'POST')
      .then(res => {
        res.code === 0 ? resolve(res) : reject(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * 获取我的所有商品
 * @returns {Promise<any>}
 */
export function getGoodList () {
  return new Promise(function (resolve, reject) {
    http.request(api.getGoodList).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的商品的具体想
 * @returns {Promise<any>}
 */
export function getGoodInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.getGoodInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存我的商品
 * @param data
 * @returns {Promise<any>}
 */
export function saveMyGoods (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveMyGoods, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 置顶我的商品
 * @param data
 * @returns {Promise<any>}
 */
export function updateMyGoods (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateMyGoods, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 删除我的商品
 * @param data
 * @returns {Promise<any>}
 */
export function deleteMyGoods (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.deleteMyGoods, params, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存我的收藏
 * @param data
 * @returns {Promise<any>}
 */
export function savecollection (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.savecollection, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
/**
 * 查看文章是否收藏
 * @param data
 * @returns {Promise<any>}
 */
export function getcollectionInfo (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getcollectionInfo, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 取消我的收藏
 * @param data
 * @returns {Promise<any>}
 */
export function deleteCollection (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.deleteCollection, params, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存点赞
 * @param data
 * @returns {Promise<any>}
 */
export function savegoodArticle (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.savegoodArticle, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 查看文章是否点赞
 * @param data
 * @returns {Promise<any>}
 */
export function getgoodArticleInfo (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getgoodArticleInfo, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 取消点赞
 * @param data
 * @returns {Promise<any>}
 */
export function deletegoodArticle (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.deletegoodArticle, params, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我子分类
 * @returns {Promise<any>}
 */
export function getChildTypeList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getChildTypeList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存分类
 * @param data
 * @returns {Promise<any>}
 */
export function saveMyType (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveMyType, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取投诉列表
 * @returns {Promise<any>}
 */
export function getComplaintList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getComplaintList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存投诉
 * @param data
 * @returns {Promise<any>}
 */
export function saveComplaint (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveComplaint, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 查询我的用户模板
 * @param data
 * @returns {Promise<any>}
 */
export function getMyContent (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getMyContent, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 查询我的用户模板
 * @param data
 * @returns {Promise<any>}
 */
export function getMyContentList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getMyContentList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存我的用户模板
 * @param data
 * @returns {Promise<any>}
 */
export function saveMyContent (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveMyContent, data, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改推送时间
 * @param data
 * @returns {Promise<any>}
 */
export function updateMyContent (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateMyContent, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

export function deleteMyContent (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.deleteMyContent, data, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 根据类型获取客服二维码的帮助
 * @param id
 * @returns {Promise<any>}
 */
export function getServiceHelp (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getServiceHelp, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我推荐的人的数据
 * @param id
 * @returns {Promise<any>}
 */
export function getMyRecommendPeople (wechatUid) {
  return new Promise(function (resolve, reject) {
    http.request(api.getMyRecommendPeople + '/' + wechatUid).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存我的上传文件
 * @param data
 * @returns {Promise<any>}
 */
export function saveUpload (file) {
  return new Promise(function (resolve, reject) {
    http.request(api.saveUpload, file, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取我的商品的具体想
 * @returns {Promise<any>}
 */
export function getPushSettingInfo (wechatUid) {
  return new Promise(function (resolve, reject) {
    http.request(api.getPushSettingInfo + '/' + wechatUid).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 微信jsSDK
 * @returns {Promise<any>}
 */
export function getJsSDK (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getJsSDK, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取硬广告
 * @returns {Promise<any>}
 */
export function getAdInfo () {
  return new Promise(function (resolve, reject) {
    http.request(api.getAdInfo).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
/**
 * 修改客户消息是否已读
 * @param data
 * @returns {Promise<any>}
 */
export function updateCustomerMessageRead (data) {
  return new Promise(function (resolve, reject) {
    http.request(api.updateCustomerMessageRead, data, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
/**
 * 获取客户消息是否已读
 * @returns {Promise<any>}
 */
export function getCustomerMessageRead (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getCustomerMessageRead, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
