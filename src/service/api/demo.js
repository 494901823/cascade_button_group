import http from '@/utils/httpRequest'
import api from '../api.js'

/**
 *  本接口为演示数据，为尽量简洁，使用统一的一个接口地址
 */

/**
 * demo菜单演示数据接口（数据信息）
 * @returns {Promise<any>}
 */
export function getDemoDataList (count) {
  return new Promise(function (resolve, reject) {
    http.request(api.demoDataList + '/' + count).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * demo菜单演示数据接口（数据列表）
 * @returns {Promise<any>}
 */
export function getDemoDataInfo () {
  return new Promise(function (resolve, reject) {
    http.request(api.demoDataInfo).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * demo菜单演示数据接口（数据列表）
 * @returns {Promise<any>}
 */
export function getDemoDataPost () {
  return new Promise(function (resolve, reject) {
    http.request(api.demoDataPost, {}, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
/**
 * demo菜单演示数据接口（数据列表）
 * @returns {Promise<any>}
 */
export function getJsSDK (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.getJsSDK, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
