import http from '@/utils/httpRequest'
import api from '../api.js'

/**
 * 内置系统模块接口对接函数定义
 * 函数名原则上来讲与对接接口的变量名一致
 * 如下：用户登陆接口名为sysLogin,其对接的接口地址变量也为api.sysLogin
 */

/**
 * 后台用户登陆
 * @param postData 提交登陆信息
 * @returns {Promise<any>}
 */
export function sysLogin (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysLogin, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 后台用户注销
 * @returns {Promise<any>}
 */
export function sysLogout () {
  return new Promise(function (resolve, reject) {
    http.request(api.sysLogout, {}, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取用户菜单及权限
 * @returns {Promise<any>}
 */
export function sysMenuNav () {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMenuNav).then(res => {
      console.log(res)
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取用户信息
 * @returns {Promise<*>}
 */
export function sysCurrentUserInfo () {
  return new Promise(function (resolve, reject) {
    http.request(api.sysCurrentUserInfo).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 修改用户密码
 * @param postData 提交的新旧密码信息
 * @returns {Promise<*>}
 */
export function sysUserUpdatePassword (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysUserUpdatePassword, postData, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 获取计划任务列表
 * @params postData 提交Get参数对象
 * @returns {Promise<*>}
 */
export function sysScheduleList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 删除计划任务
 * @param postData
 * @returns {Promise<*>}
 */
export function sysScheduleDelete (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleDelete, postData, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 暂停计划
 * @param postData
 * @returns {Promise<*>}
 */
export function sysSchedulePause (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysSchedulePause, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 恢复计划
 * @param postData
 * @returns {Promise<*>}
 */
export function sysScheduleResume (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleResume, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 执行计划
 * @param postData
 * @returns {Promise<*>}
 */
export function sysScheduleRun (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleRun, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 计划信息
 * @param id
 * @returns {Promise<*>}
 */
export function sysScheduleInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 新增计划
 * @postData postData
 * @returns {Promise<*>}
 */
export function sysScheduleSave (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleSave, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 更新计划
 * @param postData
 * @returns {Promise<*>}
 */
export function sysScheduleUpdate (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleUpdate, postData, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 任务计划日志
 * @param params
 * @returns {Promise<*>}
 */
export function sysScheduleLogList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleLogList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 任务计划日志信息
 * @param id
 * @returns {Promise<*>}
 */
export function sysScheduleLogInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysScheduleLogInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 文件存储列表
 * @returns {Promise<*>}
 */
export function sysOssList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysOssList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 删除文件
 * @param postData
 * @returns {Promise<*>}
 */
export function sysOssDelete (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysOssDelete, postData, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * oss配置
 * @returns {Promise<*>}
 */
export function sysOssConfig () {
  return new Promise(function (resolve, reject) {
    http.request(api.sysOssConfig).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * oss配置保存
 * @param postData
 * @returns {Promise<*>}
 */
export function sysOssSaveConfig (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysOssSaveConfig, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统配置列表
 * @param params
 * @returns {Promise<*>}
 */
export function sysConfigList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysConfigList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统配置列表
 * @param postData
 * @returns {Promise<*>}
 */
export function sysConfigDelete (ids) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysConfigDelete, ids, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统配置信息
 * @param id
 * @returns {Promise<*>}
 */
export function sysConfigInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysConfigInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存系统配置
 * @param postData
 * @returns {Promise<*>}
 */
export function sysConfigSave (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysConfigSave, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 更新系统配置
 * @param postData
 * @returns {Promise<any>}
 */
export function sysConfigUpdate (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysConfigUpdate, postData, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统日志列表
 * @param params
 * @returns {Promise<*>}
 */
export function sysLogList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysLogList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统菜单列表
 * @returns {Promise<*>}
 */
export function sysMenuList () {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMenuList).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 删除系统菜单
 * @param id
 * @returns {Promise<*>}
 */
export function sysMenuDelete (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMenuDelete, id, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 选择菜单
 * @param id
 * @returns {Promise<*>}
 */
export function sysMenuSelect (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMenuSelect).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统菜单信息
 * @param id
 * @returns {Promise<*>}
 */
export function sysMenuInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMenuInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存菜单
 * @param postData
 * @returns {Promise<*>}
 */
export function sysMenuSave (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMenuSave, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 更新菜单
 * @param postData
 * @returns {Promise<any>}
 */
export function sysMenuUpdate (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMenuUpdate, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统角色列表
 * @param params
 * @returns {Promise<*>}
 */
export function sysRoleList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysRoleList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
/**
 * 删除系统角色
 * @param id
 * @returns {Promise<*>}
 */
export function sysRoleDelete (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysRoleDelete, id, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统角色信息
 * @param id
 * @returns {Promise<*>}
 */
export function sysRoleInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysRoleInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存角色
 * @param postData
 * @returns {Promise<*>}
 */
export function sysRoleSave (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysRoleSave, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 更新角色
 * @param postData
 * @returns {Promise<any>}
 */
export function sysRoleUpdate (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysRoleUpdate, postData, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 选择角色
 * @param id
 * @returns {Promise<*>}
 */
export function sysRoleSelect (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysRoleSelect).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统用户列表
 * @param params
 * @returns {Promise<*>}
 */
export function sysUserList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysUserList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
/**
 * 删除系统用户
 * @param id
 * @returns {Promise<*>}
 */
export function sysUserDelete (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysUserDelete, id, 'DELETE').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 系统用户信息
 * @param id
 * @returns {Promise<*>}
 */
export function sysUserInfo (id) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysUserInfo + '/' + id).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 保存用户
 * @param postData
 * @returns {Promise<*>}
 */
export function sysUserSave (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysUserSave, postData, 'POST').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 更新用户
 * @param postData
 * @returns {Promise<any>}
 */
export function sysUserUpdate (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysUserUpdate, postData, 'PUT').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 代码生成器列表
 * @param params
 * @returns {Promise<*>}
 */
export function sysGeneratorList (params) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysGeneratorList, params).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}

/**
 * 生成代码
 * @param postData
 * @returns {Promise<*>}
 */
export function sysDownLoadMakeCode (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMakeCode, postData, 'POST', 'JSON', 'YES').then(res => {
      let contentType = res.headers['content-type']
      if (contentType.indexOf('json') > 0) {
        let blob = new Blob([res.data], {type: contentType})
        let reader = new FileReader()
        reader.readAsText(blob, 'utf-8')
        reader.onload = function () {
          res = JSON.parse(reader.result)
          reject(res)
        }
      } else {
        let disposition = res.headers['content-disposition']
        let fileName = decodeURI(disposition.substring(disposition.indexOf('filename=') + 9, disposition.length))
        let blob = new Blob([res.data], {type: contentType})
        let link = document.createElement('a')
        link.href = window.URL.createObjectURL(blob)
        link.download = fileName
        link.click()
        link.remove()
      }
    })
  })
}

/**
 * 代码生成器列表
 * @param postData
 * @returns {Promise<*>}
 */
export function sysDownLoadMakeModule (postData) {
  return new Promise(function (resolve, reject) {
    http.request(api.sysMakeModule, postData, 'POST', 'JSON', 'YES').then(res => {
      let contentType = res.headers['content-type']
      if (contentType.indexOf('json') > 0) {
        let blob = new Blob([res.data], {type: contentType})
        let reader = new FileReader()
        reader.readAsText(blob, 'utf-8')
        reader.onload = function () {
          res = JSON.parse(reader.result)
          reject(res)
        }
      } else {
        let disposition = res.headers['content-disposition']
        let fileName = decodeURI(disposition.substring(disposition.indexOf('filename=') + 9, disposition.length))
        let blob = new Blob([res.data], {type: contentType})
        let link = document.createElement('a')
        link.href = window.URL.createObjectURL(blob)
        link.download = fileName
        link.click()
        link.remove()
      }
    })
  })
}
