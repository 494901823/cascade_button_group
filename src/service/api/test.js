import http from '@/utils/httpRequest'
import api from '../api.js'

/**
 *  本接口为演示数据，为尽量简洁，使用统一的一个接口地址
 */

/**
 * demo菜单演示数据接口（数据信息）
 * @returns {Promise<any>}
 */
export function getTestList () {
  return new Promise(function (resolve, reject) {
    http.request(api.testList + '/').then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
