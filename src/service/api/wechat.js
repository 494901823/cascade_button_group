import http from '@/utils/httpRequest'
import api from '../api.js'

/**
 * 网页授权获取AccessToken
 * @returns {Promise<any>}
 */
export function getWebAuthorize (param) {
  return new Promise(function (resolve, reject) {
    http.request(api.webAuthorize, param).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
/**
 * 网页授权获取AccessToken
 * @returns {Promise<any>}
 */
export function getOauth2AccessToken (param) {
  return new Promise(function (resolve, reject) {
    http.request(api.oauth2AccessToken, param).then(res => {
      res.code === 0 ? resolve(res) : reject(res)
    })
  })
}
