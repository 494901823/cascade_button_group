// import Vue from 'vue'
import router from '@/router'
import store from '@/vuex/store'

/**
 * 获取uuid
 */
export function getUUID () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    return (c === 'x' ? (Math.random() * 16 | 0) : ('r&0x3' | '0x8')).toString(16)
  })
}

/**
 * 是否有权限
 * @param {*} key
 */
export function isAuth (key) {
  return JSON.parse(sessionStorage.getItem('permissions') || '[]').indexOf(key) !== -1 || false
}

/**
 * 树形数据转换
 * @param {*} data
 * @param {*} id
 * @param {*} pid
 */
export function treeDataTranslate (data, id = 'id', pid = 'parentId') {
  var res = []
  var temp = {}
  for (var i = 0; i < data.length; i++) {
    temp[data[i][id]] = data[i]
  }
  for (var k = 0; k < data.length; k++) {
    if (temp[data[k][pid]] && data[k][id] !== data[k][pid]) {
      if (!temp[data[k][pid]]['children']) {
        temp[data[k][pid]]['children'] = []
      }
      if (!temp[data[k][pid]]['_level']) {
        temp[data[k][pid]]['_level'] = 1
      }
      data[k]['_level'] = temp[data[k][pid]]._level + 1
      temp[data[k][pid]]['children'].push(data[k])
    } else {
      res.push(data[k])
    }
  }
  return res
}

/**
 * 清除登录信息
 */
export function clearLoginInfo () {
  // Vue.cookie.delete('token')
  sessionStorage.removeItem('token')
  store.commit('resetStore')
  router.options.isAddDynamicMenuRoutes = false
}

/**
 * 获取屏幕大小调整表格行数
 */

export function getPageSize (size, height) {
  let rowCount = 0
  switch (true) {
    case height > 0 && height <= 768:
      rowCount = 8
      break
    case height > 768 && height < 960:
      rowCount = 11
      break
    case height >= 960 && height <= 1024:
      rowCount = 13
      break
    case height > 1024 && height <= 1050:
      rowCount = 14
      break
    case height > 1050 && height <= 1080:
      rowCount = 15
      break
    default:
      rowCount = 16
  }
  switch (size) {
    case 'mini':
      return rowCount
    case 'small':
      return rowCount - 3
    default:
      return rowCount - 4
  }
}

/**
 * 查询树形数组中某个元素的路径
 * @param data
 * @param id
 * @param indexArray
 * @returns {*}
 */
export function findIndexArray (data, id, indexArray) {
  let arr = Array.from(indexArray)
  for (let i = 0, len = data.length; i < len; i++) {
    arr.push(data[i])
    if (data[i].menuId === id) {
      return arr
    }
    let children = data[i].list
    if (children && children.length) {
      let result = findIndexArray(children, id, arr)
      if (result) return result
    }
    arr.pop()
  }
  return false
}

/**
 * 判断对象是否相等（相同）
 * @param o1
 * @param o2
 * @returns {boolean}
 */
export function veq (o1, o2) {
  var props1 = Object.getOwnPropertyNames(o1)
  var props2 = Object.getOwnPropertyNames(o2)
  if (props1.length !== props2.length) {
    return false
  }
  for (var i = 0, max = props1.length; i < max; i++) {
    var propName = props1[i]
    if (o1[propName] !== o2[propName]) {
      return false
    }
  }
  return true
}

/**
 * 判断数组中是否存在相同的图书（默认图书主键为bookId）
 * @param array
 * @param object
 * @returns {boolean}
 */
export function isExist (array = [], object) {
  var itemCount = 0
  array.forEach((item) => {
    if (item.bookId === object.bookId) {
      itemCount = itemCount + 1
    }
  })
  if (itemCount > 0) {
    return true
  } else {
    return false
  }
}

/**
 * 将base64中的数据部分转换为file
 * 例：data:image/jpeg;base64,/9j/4AA
 * @param fileName 文件名
 * @param fileType 文件类型
 * @param dataUrl 组件获取base64原始数据；格式如上
 * @returns {*}
 */
export function dataURLtoFile (fileName, fileType, dataUrl) {
  let arr = dataUrl.split(',')
  let bstr = atob(arr[1])
  let n = bstr.length
  let u8arr = new Uint8Array(n)
  while (n--) {
    u8arr[n] = bstr.charCodeAt(n)
  }
  return new File([u8arr], fileName, {
    type: fileType
  })
}
