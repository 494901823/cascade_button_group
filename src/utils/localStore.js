export default {
  save: function (key, item) {
    window.localStorage.setItem(key, JSON.stringify(item))
  },
  fetch: function (key) {
    return JSON.parse(window.localStorage.getItem(key)) || []
  },
  removeAll: function () {
    console.log('删除数据')
    window.localStorage.clear()
  },
  removeItem: function (key) {
    window.localStorage.removeItem(key)
  },
  storageLength: function () {
    return window.localStorage.length
  },
  updateItem: function (item, key) {
    window.localStorage[key] = JSON.stringify(item)
  },
  enumObj: function () {
    for (let i = 0; i < window.localStorage.length; i++) {
      let key = window.localStorage.key(i)
      console.log('key:' + key + 'Value:' + window.localStorage.getItem(key))
    }
  }
}
