export default {
  getUrlParams: (name) => {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' +
      '([^&;]+?)(&|#|;|$)').exec(location.href || [''])[1].replace(/\+/g,
      '%20'))) || null
  },
  getUrlKey: function (name) {
    /* eslint-disable */
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.href) || [, ""])[1].replace(/\+/g, '%20')) || null
  },
  imgUrlFun: function(str){
    let data = '';
    str.replace(/<img [^>]*src=['"]([^'"]+)[^>]*>/, function (match, capture) {
      data =  capture;
    });
    return data
  }
}
