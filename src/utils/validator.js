import { isEmail, isMobile } from '@/utils/validate'
import {isIsbn} from './validate'
// 业务接口的例子
// import * as demo from '@/service/api/demo'

/**
 * 编写验证器可以调用serivce中的接口，进行实时验证
 * 所有验证情况都必须有callback，即使是正常情况也要返回空的callback
 * 否则验证的最终结果不会是true
 */

/**
 * 验证邮件格式的验证器，可以添加手机号重复的规则
 * @param rule
 * @param value
 * @param callback
 */

function validateEmail (rule, value, callback) {
  if (value && !isEmail(value)) {
    callback(new Error('邮件格式不正确'))
  } else {
    callback()
  }
}

/**
 * 验证手机号的规则，可以添加验证手机号重复的规则
 * @param rule
 * @param value
 * @param callback
 */
function validateMobile (rule, value, callback) {
  if (value && !isMobile(value)) {
    callback(new Error('手机号格式不正确'))
  } else {
    callback()
  }
}

/**
 * 验证员工编号是否重复：业务接口的例子
 * @param rule
 * @param value
 * @param callback
 */
// function validateUserIdUnique (rule, value, callback) {
//   if (value) {
//     demo.demoGridsInfo(value).then(res => {
//       if (res.code === 0) {
//         if (Object.keys(res.data).length === 0) {
//           callback()
//         } else {
//           callback(new Error('此员工号已经存在'))
//         }
//       }
//       callback(new Error('系统错误，请联系管理员'))
//     })
//   }
// }

// demo 演示规则
function validateBookIsbn (rule, value, callback) {
  if (value && !isIsbn(value)) {
    callback(new Error('输入的ISBN不正确'))
  } else {
    callback()
  }
}
// 需求分析演示的时候，限制登陆用户，正式版注释掉
function validateDemoUser (rule, value, callback) {
  if (value && value !== 'demo') {
    callback(new Error('输入的用户名不正确'))
  } else {
    callback()
  }
}
// 需求分析演示的时候，限制登陆用户密码，正式版注释掉
function validateDemoPassword (rule, value, callback) {
  if (value && value !== 'demo@123') {
    callback(new Error('输入的密码不正确'))
  } else {
    callback()
  }
}

export default {
  validateEmail,
  validateMobile,
  // validateUserIdUnique,
  validateBookIsbn,
  validateDemoUser,
  validateDemoPassword
}
